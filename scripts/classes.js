export class Weather {
  constructor(day, img, temp, minTemp, maxTemp) {
    this.day = day;
    this.img = img;
    this.temp = temp;
    this.minTemp = minTemp;
    this.maxTemp = maxTemp;
  }
}
