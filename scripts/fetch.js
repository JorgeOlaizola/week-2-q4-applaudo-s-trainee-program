import { search } from './elements.js';
import { setWeather, setSearchItem } from './weatherControllers.js';

let localStorage = JSON.parse(window.localStorage.getItem('city'));
export let weatherInfo;

export const fetchWeather = async function (id) {
  let data
  let url = `https://www.metaweather.com/api/location/${id}/`
  // If there is an id, it´s a custom fetch
  if(id) data = fetch(url)
  // If there isn´t an id, we look in the localStorage for previous searchs
  else if (localStorage){
    weatherInfo = localStorage
     return setWeather(localStorage)
    }
  // If localStorage it´s empty, it´s the first fetch --> San Francisco by Default (this code will execute only once)
  else data = fetch('https://www.metaweather.com/api/location/2487956/');

  data
  .then(response => response.json())
  .then(data => {
    // Saving the info in a variable for the modal extra info
    weatherInfo = data
    return setWeather(data)
  })
  .catch(error => console.log(error))
};

export const searchFetch = (event) => {
  if(event.key === 'Enter') return searchClick()
  if(event.target.value.length > 0){
    const url = `https://www.metaweather.com/api/location/search/?query=${event.target.value}`
    fetch(url)
      .then((response) => response.json())
      .then((response) => setSearchItem(response))
      .catch((error) => console.log(error));
  }
  else{
    setSearchItem([])
  }
};

export const searchClick = () => {
  if(!search.value) return
  const url = `https://www.metaweather.com/api/location/search/?query=${search.value}`
  fetch(url)
  .then((response) => response.json())
  .then((response) => {
  if(response.length > 0) fetchWeather(response[0].woeid)
})
  .catch((error) => console.log(error));
}
