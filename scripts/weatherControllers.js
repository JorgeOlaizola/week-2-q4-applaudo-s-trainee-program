import { fetchWeather } from './fetch.js';
import { city, searchContainer } from './elements.js'
import { openModal } from './modalControllers.js';
import { days } from './utils.js'

export const setWeather = function (data) {
  //  Setting city
  city.city.innerHTML = data.title;
  city.country.innerHTML = data.parent.title;
  city.timezone.innerHTML = data.timezone + `(${data.timezone_name})`;
  //  Setting weathers
  for (let i = 0; i < 5; i++) {
    city.weathers[i].day.innerHTML = days[new Date(data.consolidated_weather[i].applicable_date).getDay()]
    city.weathers[i].img.src = `https://www.metaweather.com/static/img/weather/${data.consolidated_weather[i].weather_state_abbr}.svg`;
    city.weathers[i].temp.innerHTML = Math.round(data.consolidated_weather[i].the_temp) + '°';
    city.weathers[i].minTemp.innerHTML = Math.round(data.consolidated_weather[i].min_temp) + '°';
    city.weathers[i].maxTemp.innerHTML = Math.round(data.consolidated_weather[i].max_temp) + '°';
  }
  // Saving city in localStorage
  localStorage.setItem('city', JSON.stringify(data));
};

export const setSearchItem = function (cities) {
  // Slicing the array for only 5 results
  cities = cities.slice(0, 5);

  // Deleting previous search-items
  for (let i = 0; searchContainer.childNodes.length; i++) {
    searchContainer.removeChild(searchContainer.lastChild);
  }

  // Creating a search-container-item for each result
  if (cities.length > 0) {
    cities.forEach((city) => {
      let newDiv = document.createElement('div');
      newDiv.className = 'search-container-item';
      newDiv.style.cursor = 'pointer'
      newDiv.innerHTML = city.title;
      newDiv.id = city.woeid;
      newDiv.onclick = () => {
        fetchWeather(newDiv.id);
      };
      searchContainer.appendChild(newDiv);
    });
  } else {
    let newDiv = document.createElement('div');
    newDiv.className = 'search-container-item';
    newDiv.style.color = 'gray';
    newDiv.innerHTML = 'No results found';
    searchContainer.appendChild(newDiv);
  }
};
