import {
  blurFunction,
  focusFunction,
  debounceFunction,
  throttleFunction,
  randomBackground,
  backgrounds,
} from './utils.js';
import {
  closeModal,
  closeModalClickingOutside,
  closeModalWithEscape,
  openModal,
} from './modalControllers.js';
import { closeModalBtn, search, searchBtn } from './elements.js';
import { fetchWeather, searchFetch, searchClick } from './fetch.js';

// Initial actions

fetchWeather();
randomBackground(backgrounds);

setInterval(() => {
  randomBackground(backgrounds);
}, 8000);

// Event listeners

search.addEventListener('focus', focusFunction);
search.addEventListener('blur', debounceFunction(blurFunction, 200));
search.addEventListener('keyup', debounceFunction(searchFetch, 400));
searchBtn.addEventListener('click', throttleFunction(searchClick, 1000));

// Modal listeners

closeModalBtn.addEventListener('click', closeModal);
window.addEventListener('keyup', closeModalWithEscape);
window.addEventListener('click', closeModalClickingOutside);

for (let i = 0; i < 5; i++) {
  let weather = document.getElementById(`weather${i}`);
  weather.addEventListener('click', (event) => { openModal(event, `${i}`) });
}
