import { modal } from './elements.js';
import { weatherInfo } from './fetch.js';
import { days } from './utils.js';

export const openModal = function (event, index) {
    modal.title.innerHTML = weatherInfo.title + ' - ' +  days[new Date(weatherInfo.consolidated_weather[index].applicable_date).getDay()] + ` (${weatherInfo.consolidated_weather[index].applicable_date.split('-').reverse().join('/')})`
    modal.img.src = `https://www.metaweather.com/static/img/weather/${weatherInfo.consolidated_weather[index].weather_state_abbr}.svg`;
    modal.weather.innerHTML = `Weather: ${weatherInfo.consolidated_weather[index].weather_state_name}`
    modal.temp.innerHTML = `Temperature: ${Math.round(weatherInfo.consolidated_weather[index].the_temp)}°`
    modal.min.innerHTML = `Min temperature: ${Math.round(weatherInfo.consolidated_weather[index].min_temp)}°`
    modal.max.innerHTML = `Max temperature: ${Math.round(weatherInfo.consolidated_weather[index].max_temp)}°`
    modal.humidity.innerHTML = `Humidity: ${weatherInfo.consolidated_weather[index].humidity} %`
    modal.windSpeed.innerHTML = `Wind Speed: ${Math.round(weatherInfo.consolidated_weather[index].wind_speed)} m/s`
    modal.windDirection.innerHTML = `Wind Direction: ${Math.round(parseInt(weatherInfo.consolidated_weather[index].wind_direction))}`
    modal.airPressure.innerHTML = `Air Pressure: ${weatherInfo.consolidated_weather[index].air_pressure}`
    modal.predictability.innerHTML = `Predictability: ${weatherInfo.consolidated_weather[index].predictability}`
    modal.modal.style.display = 'block';
};

export const closeModal = function () {
  modal.modal.style.display = 'none';
};

export const closeModalWithEscape = function (event) {
  if (modal.modal.style.display === 'block' && event.key === 'Escape')
    modal.modal.style.display = 'none';
};

export const closeModalClickingOutside = function (event) {
  if (modal.modal.style.display === 'block' && event.target === modal.modal)
    modal.modal.style.display = 'none';
};
