import { Weather } from './classes.js';
import { openModal } from './modalControllers.js';

// CITY

const weathers = [];

for (let i = 0; i < 5; i++) {
  let day = document.getElementById(`day${i}`);
  let img = document.getElementById(`img${i}`);
  let temp = document.getElementById(`temp${i}`);
  let minTemp = document.getElementById(`min${i}`);
  let maxTemp = document.getElementById(`max${i}`);
  let card = document.getElementById(`weather${i}`);
  weathers.push(new Weather(day, img, temp, minTemp, maxTemp));
}

export const city = {
  country: document.getElementById('country'),
  city: document.getElementById('city'),
  timezone: document.getElementById('timezone'),
  weathers,
};

// ELEMENTS

export const search = document.getElementById('search');
export const searchContainer = document.getElementById('searchContainer');
export const searchBtn = document.getElementById('searchBtn');

//  MODAL ELEMENTS

export const modal = {
  modal: document.getElementById('modal'),
  img: document.getElementById('modal-img'),
  content: document.getElementsByClassName('modal-content')[0],
  title: document.getElementsByClassName('modal-title')[0],
  weather: document.getElementById('modal-weather'),
  temp: document.getElementById('modal-temp'),
  min: document.getElementById('modal-min'),
  max: document.getElementById('modal-max'),
  predictability: document.getElementById('predictability'),
  humidity: document.getElementById('humidity'),
  windSpeed: document.getElementById('wind-speed'),
  airPressure: document.getElementById('air-pressure'),
  windDirection: document.getElementById('wind-direction'),
};
export const closeModalBtn = document.getElementById('close');
