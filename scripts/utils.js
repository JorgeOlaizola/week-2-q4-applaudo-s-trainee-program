import { searchContainer } from './elements.js';

export const blurFunction = function () {
  searchContainer.style.display = 'none';
};

export const focusFunction = function () {
  searchContainer.style.display = 'flex';
};

export const debounceFunction = function (fn, delay) {
  let timeoutID;
  return function (...args) {
    if (timeoutID) {
      clearTimeout(timeoutID);
    }

    timeoutID = setTimeout(() => {
      fn(...args);
    }, delay);
  };
};

export const throttleFunction = function (fn, delay) {
  let last = 0;
  return function (...args) {
    const now = new Date().getTime();
    if(now - last < delay) {
      return
    }
    last = now;
    return fn(...args)
  }
}

const body = document.getElementById('root')

export const randomBackground = function (obj) {
  let randomNumber = Math.round(Math.random() * Object.keys(obj).length - 1)
  obj[randomNumber] ? body.style.background = `URL(${obj[randomNumber]})` : body.style.background = 'URL(https://images.unsplash.com/photo-1567190564513-6fb2fb610318?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1074&q=80)'
  body.style.backgroundColor =  'rgb(0, 0, 0)'
  body.style.backgroundBlend = -'mode lighten'
  body.style.backgroundRepeat =  'no-repeat'
  body.style.backgroundSize=  'cover'
  body.style.backgroundPosition = 'center center'
  body.style.backgroundAttachment =  'fixed'
}

export const days = {
  0: 'Monday',
  1: 'Tuesday',
  2: 'Wednesday',
  3: 'Thursday',
  4: 'Friday',
  5: 'Saturday',
  6: 'Sunday'
}

export const backgrounds = {
  0: 'https://res.cloudinary.com/jorgeleandroolaizola/image/upload/v1632502693/Pngtree_blue_sky_and_white_clouds_1055722_nztobf.jpg',
  1: 'https://images.unsplash.com/photo-1567190564513-6fb2fb610318?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1074&q=80',
  2: 'https://images.unsplash.com/photo-1565012030177-5ea224a04b84?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1175&q=80',
  3: 'https://images.unsplash.com/photo-1500674425229-f692875b0ab7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80',
  4: 'https://images.unsplash.com/photo-1548774593-2bfe3947cc30?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1230&q=80',
  5: 'https://images.unsplash.com/photo-1566010503302-2564ae0d47b6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80',
  6: 'https://images.unsplash.com/photo-1544039161-b0c20826c6f6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80',
  7: 'https://images.unsplash.com/photo-1565130838609-c3a86655db61?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80',
  8: 'https://images.unsplash.com/photo-1563669172719-6d3fffd82e36?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1074&q=80',
  9: 'https://images.unsplash.com/photo-1611157084674-b6073b2a218a?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80',
}